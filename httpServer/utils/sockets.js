// Socket io for pushing periodic events.
const io = require('socket.io')(require(global.appRoot + '/bin/www'), { cookie: false });
const httpLog = require(global.appRoot + '/loggers/httpLogger.js');
const config = require('config');

const net = require('net');

io.on('connection', function(socket){
	httpLog.info('Client connected from: ' + socket.handshake.address);

	socket.on('reloadReq', () => {
		httpLog.info('reloadReq received, will broadcast reload to all clients.');
		socket.emit('reload');
	});

	socket.emit('greeting', {
		message: 'Greeting from RF controller.',
		controller: config.controller,
		role: process.env.role
	});

	socket.on('message', data=>{
		console.log(`Msg received: ${data}`);
	});

	socket.on('connectSGServer', ()=>{
		connectSGServer();
	});

	setInterval(() => {
		const date = new Date();
		const dateStr = date.toDateString() + ' - ' + date.toLocaleTimeString();
		socket.emit('timeStamp', { timeStamp: dateStr });
	},
	1000 // Interval: 1 second
	);


// 	config.get("sgStatusPollingPeriod")
});

function connectSGServer(){
	const socket_to_SG = net.connect({
		//host: '0.0.0.0',
		host: '143.248.185.74',
		port: 5050
	});

	socket_to_SG.setEncoding('utf-8');

	socket_to_SG.on('connect', ()=>{
		console.log('Connected to the server.');

		setTimeout(()=>{
			socket_to_SG.write('quit');
		}, 1000);
	});

	socket_to_SG.on('data', data=>{
		console.log(data);
	});

	socket_to_SG.on('close', ()=>{
		console.log('close');
	});

	socket_to_SG.on('error', err=>{
		console.log(`On error: ${err.code}`);
	});
}


// console.log(net);
module.exports = {
	io,
	net
};
