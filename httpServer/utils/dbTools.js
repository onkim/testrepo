const db = require(global.appRoot + '/utils/db');

function getSG(){
	const collection = db.get().db('rf').collection('sgTools');
	const result = collection.find().sort({ $natural: -1 }).toArray();
	return result;
}

module.exports = {
	getSG: getSG
}
