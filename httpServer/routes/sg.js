var express = require('express');
var router = express.Router();
const httpLog = require(global.appRoot + '/loggers/httpLogger.js');
const sgStatusLog = require(global.appRoot + '/loggers/sgStatusLogger.js');
const exec = require('util').promisify(require('child_process').exec);
const dbTools = require(global.appRoot + '/utils/dbTools');

router
	.get('/', async(req, res, next) => {
		try{
			httpLog.info('Waiting dbTools.getSG...');
			const currentSG = await dbTools.getSG();
			res.json(currentSG);
			httpLog.info('currentSG: ' + currentSG);
		}
		catch(e){
			next(e);
			httpLog.error(e);
		}
	})
	.post('/', (req, res, next) => {
		var outp = JSON.stringify(req.body);
		httpLog.info('sgControl Request: ' + outp);
		sgStatusLog.info('sgControl get going: ' + outp);
		// const cmd = 'python3 ' + global.appRoot + '/../hwInterface/sgComm.py';
		const cmd = '. ' + global.appRoot + '/../hwInterface/sendCmd.sh ' + 'sumTwo 6 17';
		httpLog.info('Exec this cmd: ' + cmd);

		exec(cmd).then( (data) => {
			httpLog.info('Request done?');
			res.sendStatus(200);
			httpLog.info('data: ' + data.stdout);
		}).catch( (err) => {
			httpLog.error('Error occurred: ' + err.stderr);
			res.status(500).send(err.stderr);
		});
	});

module.exports = router;
