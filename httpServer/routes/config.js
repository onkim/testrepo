var express = require('express');
var router = express.Router();
const httpLog = require(global.appRoot + '/loggers/httpLogger.js');
const sgStatusLog = require(global.appRoot + '/loggers/sgStatusLogger.js');
const exec = require('util').promisify(require('child_process').exec);
const dbTools = require(global.appRoot + '/utils/dbTools');
const config = require('config');

router
	.get('/', async(req, res, next) => {
		try{
			httpLog.info('Waiting to send config...');
			res.send(config);
		}
		catch(e){
			next(e);
			httpLog.error(e);
		}
	});

module.exports = router;
