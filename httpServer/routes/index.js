var express = require('express');
var router = express.Router();
const config = require('config');

// Get homepage.
router.get('/',	function(req, res, next){
	res.render('index', {
		title: 'Express',
		config: config,
		constants: {
			rows: [ ...Array(config.html.numberOfRows+1).keys() ].slice(1) 
		}
	});
});

module.exports = router;
