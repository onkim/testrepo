var sgStatusOptions = {
	file: {
		daily: false,
		name: "sgStatus",
		outputName: "sgStatus"
	},
	console: {
		enable: true
	},
	db: {
		name: "sgStatus"
	},
	dbOnline:{
		enable: true,
		name: "sgStatus",
		nPoints: 1
	}
};

var sgStatusLog = require(global.appRoot + "/loggers/baseLogger.js")(sgStatusOptions);

// Stream.write is needed for streaming morgan log to winston outputs
sgStatusLog.stream = {
	write: function(message, encoding){
		sgStatusLog.info(message.trim());
	}
};

module.exports = sgStatusLog;

