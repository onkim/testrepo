function mod(n, m){
	// To always return a positive value.
	return ((n % m) + m) % m;
}

function calculateWaveParams(quad, row){
	console.log('input values are: ' + quad + ' and ' + row);
	let freq 		= Number.parseFloat( getWaveParamElement('Freq', quad, row).value )/1e3; // in MHz
	let startTime 	= Number.parseFloat( getWaveParamElement('StartTime', quad, row).value ); // in us
	let nPeriods 	= Number.parseFloat( getWaveParamElement('NPeriods', quad, row).value );
	let delayPhase 	= Number.parseFloat( getWaveParamElement('DelayPhase', quad, row).value )*Math.PI/180; // in rad

	// Duration
	let duration = nPeriods/freq; // in us
	getWaveParamElement('Duration', quad, row).innerHTML = isNaN(duration) ? '-' : duration.toFixed(3);

	// ActualTime
	let omega = 2*Math.PI*freq; // in Mrad/s
	let period = 1/freq; // in us
	let actualStartTime = startTime + delayPhase/omega; // in us
	let actualEndTime = actualStartTime + nPeriods*period; // in us
	let actualTimeElement = getWaveParamElement('ActualTime', quad, row);
	actualTimeElement.startTime = actualStartTime;
	actualTimeElement.endTime = actualEndTime;
	actualTimeElement.innerHTML = isNaN(actualStartTime) ? '- / -' : actualStartTime.toFixed(2) + ' / ' + actualEndTime.toFixed(2);

	// RefPhase
	let refPhase = mod(-actualStartTime/period*360, 360); // in deg
	getWaveParamElement('RefPhase', quad, row).innerHTML = isNaN(refPhase) ? '-' : refPhase.toFixed(3);
}
