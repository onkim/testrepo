const baseUrl = 'http://' + document.location.hostname + ':' + document.location.port;
console.log('baseUrl is: ' + baseUrl);
var socket = io.connect(baseUrl);

socket.on(
	'greeting',
	function(data){
		window.controller = data.controller;
		window.role = data.role;
		console.info(data.message);
		console.info('Controller in use is: ', window.controller);
		console.info('Role of this GUI is: ', data.role);
	}
);

socket.on(
	'reload',
	() => {
		console.log('Forced reload message received.');
		window.location.reload(true);
	}
);

function forceReload(){
	socket.emit('reloadReq');
}

socket.on(
	'timeStamp',
	(data) => {
		$('#clock').html('<h4> Server time: ' + data.timeStamp + '</h4>');
	}
);

// const soc = io.connect('143.248.185.74:5050');
// console.log(soc);
// soc.on('connection', sck=>{
// 	sck.send('quit');
// 	sck.emit('quit');
// 	sck.on('message', data=>{
// 		console.log(data);
// 	});
// });

socket.emit('message', {data: 'Hello, my msg.'});







function assignWindowProperties(){
	window.quads = ['Q1', 'Q2', 'Q3', 'Q4'];
	window.quadPlates = ['Q1T', 'Q1I', 'Q1O', 'Q2T', 'Q2I', 'Q2O', 'Q3T', 'Q3I', 'Q3O', 'Q4T', 'Q4I', 'Q4O', 'QB'];

	let i = 0;
	while ( document.getElementById('checkboxQ1_On' + ++i) ){}
	window.rows = [ ...Array(i).keys() ].slice(1);
}

function getWaveParamElement(name, quad, row, jquery=false){
	// checkbox - On
	// select 	- Mode
	// input	- Freq, StartTime, NPeriods, Amplitude, DelayPhase
	// label	- Duration, ActualTime, RefPhase 
	
	let suffix = quad + '_' + name + row;
	if (name == 'On'){
		return jquery ? $('#checkbox' + suffix) : document.getElementById('checkbox' + suffix);
	}
	else if (name == 'Mode'){
		return jquery ? $('#select' + suffix) : document.getElementById('select' + suffix);
	}
	else if (['Freq', 'StartTime', 'NPeriods', 'Amplitude', 'DelayPhase'].includes(name)){
		return jquery ? $('#input' + suffix) : document.getElementById('input' + suffix);
	}
	else if (['Duration', 'ActualTime', 'RefPhase'].includes(name)){
		return jquery ? $('#label' + suffix) : document.getElementById('label' + suffix);
	}
	else{
		throw 'Wrong input name for the function getWaveformParams!';
	}
}

window.onload = () => {
	initWaveformPlots();
};

assignWindowProperties();

$(() => {
	$('#btnWaveform').click( ()=>{ sendWaveform('All'); } );
	$('#btnOutputOn').click( ()=>{ turnOutput('All', 'On'); } );
	$('#btnOutputOff').click( ()=>{ turnOutput('All', 'Off'); } );

	for (let ch of window.quadPlates){
		$('#btnWaveform' + ch).click( ()=>{ sendWaveform(ch); } );
		$('#btnOutput' + ch).click( ()=>{ turnOutput(ch, 'Toggle'); } );
	}
	
	for (let quad of window.quads){
		for (let row of window.rows){
			getWaveParamElement('On', quad, row, true).click( ()=>{ updateWaveformPlots(quad); });
			getWaveParamElement('Freq', quad, row, true).on('input', ()=>{ calculateWaveParams(quad, row); });
			getWaveParamElement('StartTime', quad, row, true).on('input', ()=>{ calculateWaveParams(quad, row); });
			getWaveParamElement('NPeriods', quad, row, true).on('input', ()=>{ calculateWaveParams(quad, row); });
			getWaveParamElement('DelayPhase', quad, row, true).on('input', ()=>{ calculateWaveParams(quad, row); });
		}
	}
});


