function turnOutput(channel, target){
	return new Promise( (resolve, reject) => {
		$.ajax({
			type: 'POST',
			url: baseUrl + '/sg',
			data: { func: 'turnOutput', channel: channel, target: target },
			success: (res) => {
				console.info(res);
				resolve(true);
			},
			error: (err, stat) => {
				resolve(false);
				alert('My error, ' + err.responseText);
			}
		});
	});
};

function sendWaveform(channel){
	return new Promise( (resolve, reject) => {
		$.ajax({
			type: 'POST',
			url: baseUrl + '/sg',
			data: { func: 'sendWaveform', channel: channel },
			success: (res) => {
				console.info(res);
				resolve(true);
			},
			error: (err, stat) => {
				resolve(false);
				alert('My error, ' + err.responseText);
			}
		});
	});
};
