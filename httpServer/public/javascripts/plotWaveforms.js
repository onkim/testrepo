function arange(start, stop, step=1){
	step = step || 1;
	let arr = [];
	for (let i=start; i<stop; i+=step){
		arr.push(i);
	}
	return arr;
}

Array.prototype.sumArray = function (arr){
	return this.map((val, ind) => val + arr[ind]);
};

Array.prototype.scaleArray = function (scalar){
	return this.map((val) => scalar*val);
}

function initWaveformPlots(){
	let layout = {
//		title: 'hi',
		grid: {
			rows: 2,
			columns: 2,
			pattern: 'independent'
		},
		margin: {
//			l: 0,
//			r: 0,
			t: 15,
			b: 45,
			pad: 0,
			autoexpand: true
		},
		showlegend: false,
		// xaxis : { title: 'Time [us]' },
		// xaxis2: { title: 'Time [us]' },
		xaxis3: { title: 'Time [us]' },
		xaxis4: { title: 'Time [us]' },
		yaxis : { title: 'Top' },
		yaxis2: { title: 'Inner' },
		yaxis3: { title: 'Bottom' },
		yaxis4: { title: 'Outer' }
	};
	
	const timeStep = 0.01; // in us
	window.timeRange = arange(0, document.getElementById('inputWaveformSpan').value, timeStep);
	const zeros = new Array(window.timeRange.length).fill(0);
	
	let top    = { x: window.timeRange, y: zeros, mode: 'lines', name: 'Top'};
	let bottom = { x: window.timeRange, y: zeros, mode: 'lines', name: 'Bottom', xaxis: 'x3', yaxis: 'y3' };
	let inner  = { x: window.timeRange, y: zeros, mode: 'lines', name: 'Inner' , xaxis: 'x2', yaxis: 'y2' };
	let outer  = { x: window.timeRange, y: zeros, mode: 'lines', name: 'Outer' , xaxis: 'x4', yaxis: 'y4' };

	window.waveformPlotly = {};
	['Q1', 'Q2', 'Q3', 'Q4'].forEach((val)=>{
		window.waveformPlotly[val] = [ new Object(top), new Object(bottom), new Object(inner), new Object(outer) ];

		Plotly.newPlot(
			document.getElementById(`waveformPlots${val}`),
			window.waveformPlotly[val],
			layout,
			{ responsive: true }
		);
	});
}

function updateWaveform(waveform, mode, freq, actualStartTime, actualEndTime, amplitude){
	// Units: freq (MHz), time (us)
	const omega = 2*Math.PI*freq; // in Mrad/s
	const period = 1/freq; // in us
	let amps = amplitude.split("/").map(val=>Number.parseFloat(val));
	while (amps.length < 4) amps = amps.concat(amps);
	
	const sin_main = window.timeRange.map(t => (t > actualStartTime && t < actualEndTime) ? Math.sin(omega*(t - actualStartTime)) : 0);
	const sin_with_tail = sin_main.sumArray( window.timeRange.map(t => (t > actualEndTime && t < actualEndTime + period/2) ? 0.5*Math.exp(-5*(t - actualEndTime)/(period/2))*Math.sin(omega*(t - actualStartTime)) : 0) );
	
	switch (mode){
		case 'HD':
			waveform.inner  = waveform.inner .sumArray( sin_with_tail.scaleArray( amps[0]) );
			waveform.outer  = waveform.outer .sumArray( sin_with_tail.scaleArray(-amps[1]) );
			break;
		case 'VD':
			waveform.top    = waveform.top   .sumArray( sin_with_tail.scaleArray( amps[0]) );
			waveform.bottom = waveform.bottom.sumArray( sin_with_tail.scaleArray(-amps[1]) );
			break;
		case 'Q':
			waveform.top    = waveform.top   .sumArray( sin_with_tail.scaleArray( amps[0]) );
			waveform.bottom = waveform.bottom.sumArray( sin_with_tail.scaleArray( amps[1]) );
			waveform.inner  = waveform.inner .sumArray( sin_with_tail.scaleArray(-amps[2]) );
			waveform.outer  = waveform.outer .sumArray( sin_with_tail.scaleArray(-amps[3]) );
			break;
	}
}

function updateWaveformPlots(quad){
	// on, mode, freq, startTime, nPeriods, duration, actualTime, amplitude, delayPhase, refPhase
	console.log('Called! quad: ' + quad);
	let waveform = {
		top   : new Array(window.timeRange.length).fill(0),
		bottom: new Array(window.timeRange.length).fill(0),
		inner : new Array(window.timeRange.length).fill(0),
		outer : new Array(window.timeRange.length).fill(0)
	};
	for (let row of window.rows){
		if (getWaveParamElement('On', quad, row).checked === true){
			['Mode', 'Freq', 'StartTime', 'NPeriods', 'Amplitude', 'DelayPhase'].forEach((val)=>{
				getWaveParamElement(val, quad, row).disabled = true;
			});
			
			let mode = getWaveParamElement('Mode', quad, row).value;
			let freq = Number.parseFloat(getWaveParamElement('Freq', quad, row).value)/1e3;
			let start = getWaveParamElement('ActualTime', quad, row).startTime;
			let end = getWaveParamElement('ActualTime', quad, row).endTime;
			let amplitude = getWaveParamElement('Amplitude', quad, row).value;
			console.log(mode, freq, start, end, amplitude);
			updateWaveform(waveform, mode, freq, start, end, amplitude);
		}
		else {
			['Mode', 'Freq', 'StartTime', 'NPeriods', 'Amplitude', 'DelayPhase'].forEach((val)=>{
				getWaveParamElement(val, quad, row).disabled = false;
			});
		}
	}
	
	['top', 'bottom', 'inner', 'outer'].forEach((val, ind)=>{
		window.waveformPlotly[quad][ind].y = waveform[val];
	});
	
	Plotly.redraw(document.getElementById(`waveformPlots${quad}`));
}
