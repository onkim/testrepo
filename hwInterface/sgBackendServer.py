#! /usr/bin/python3

import zerorpc
import pyvisa
from time import sleep

address = "tcp://0.0.0.0:4242"

class BackendRPC(object):
	def __init__(self):
		self.rm = pyvisa.ResourceManager()
		self.resources = self.rm.list_resources()
		self.sg = self.rm.open_resource(self.resources[0])

	def sendCmd(self, cmd, read=True):
		self.sg.write(cmd)
		sleep(0.5)
		if read:
			outp = self.sg.read()
			return outp

	def turnON(self):
		self.sendCmd(self.sg, "*IDN?")

		self.sendCmd(self.sg, "C1:OUTP ON", read=False)
		self.sendCmd(self.sg, "C1:OUTP?")



if __name__ == "__main__":
	s = zerorpc.Server(BackendRPC())
	s.bind(address)
	s.run()
