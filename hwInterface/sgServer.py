#! /usr/bin/python3
#-*- codiing: utf-8 -*-

import socket
import select
import numpy as np
import pyvisa
from datetime import datetime
from time import sleep
import os, sys

"""
Socket settings
"""
# Host and port settings for the socket server.
Host = "0.0.0.0"
Port = 5050
RecvBuffsize = 2048

"""
Signal generator and Pyvisa settings
"""
use_python_wrapper = True # Python wrapper to open the pyvisa resource manager.
idVendor, idProduct = 1535, 2593 # Teledyne-LeCrody WaveStation2012.
QuadPlateEntries = np.array(["Q1T", "Q1I", "Q1O", "Q2T", "Q2I", "Q2O", "Q3T", "Q3I", "Q3O", "Q4T", "Q4I", "Q4O", "QB"])
WaveformMaximumThreshold = 0.002 # The maximum absolute amplitude of a signal to be sent to the SGs should be higher than the threshold.

def sendCommand(device, cmd, query="", sleepTime=0.5):
    if cmd:
        device.write(cmd); sleep(sleepTime)
    if query:
        device.write(query)
        output = device.read()
        return output

class SGBackendServer(object):
    def __init__(self):
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.resourceManager = pyvisa.ResourceManager("@py" if use_python_wrapper else "")
        self.SG_Amps = np.genfromtxt(f"{self.path}/sg_amp_connection.dat", dtype="str", skip_header=1) # Load the signal generators - amplifiers connection information.
        self.SG_connection = [ {"label":label,
								"serial":serial,
								"ch1":ch1, 
								"ch2":ch2, 
								"ch1index":np.where(QuadPlateEntries == ch1)[0][0] if ch1 in QuadPlateEntries else None,
								"ch2index":np.where(QuadPlateEntries == ch2)[0][0] if ch2 in QuadPlateEntries else None,
								"rm":pyvisa.ResourceManager("@py" if use_python_wrapper else ""), 
								"device":0} for (label, serial, ch1, ch2) in self.SG_Amps ]
        self.SG_plateIndices = [] # Mapping from the quad plate to (SG, channel).
        for plate in QuadPlateEntries:
            for i, sg in enumerate(self.SG_connection):
                if plate == sg["ch1"]:
                    self.SG_plateIndices.append((i, 1)) # i-th sg, ch1
                    break
                elif plate == sg["ch2"]:
                    self.SG_plateIndices.append((i, 1)) # i-th sg, ch2
                    break
        self.SG_plateIndices = np.array(self.SG_plateIndices)

        self.openSocketServer()

    def openSocketServer(self):
        # Make a socket object: (address family: IPv4 (AF_INET), type: TCP (SOCK_STREAM)).
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # To handle the WinError 10048 when the port is occupied.
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Open the socket server.
        self.server_socket.bind((Host, Port))
        self.server_socket.listen()

        # Run the socket server.
        while True:
            ready_to_read, ready_to_write, in_error = select.select([self.server_socket], [self.server_socket], [])
            # Get client information.
            client_socket, client_addr = self.server_socket.accept()
            print(f"Connected by {client_addr}.")

            data = client_socket.recv(RecvBuffsize)
            print(f"Received from {client_addr}: {data.decode()}")

            if data.decode() == "shutdownServer":
                break
        
        client_socket.close()
        self.server_socket.close()
    
    def getConfig(self):
        # self.arwvFreq = 1e6/inputWaveformSpan
        pass

    def initSG(self, sg):
        if not sg["device"]:
            sg["device"] = sg["rm"].open_resource(f"USB0::{idVendor}::{idProduct}::{sg['serial']}::0::INSTR")
        try:
            rosc = sendCommand(sg["device"], "ROSC EXT", "ROSC?") # Set reference clock "external".
            if "EXT" not in rosc: raise Exception("ROSC setting has not been done properly.")

            for i, ch in enumerate(["ch1index", "ch2index"]):
                if sg[ch] != None:
                    output = sendCommand(sg["device"], f"C{i+1}:OUTP LOAD, 50, PLRT, NOR", f"C{i+1}:OUTP?") # Set output impedance 50 Ohm, polarity normal.

        except Exception as e:
            print(f"Exception has occurred: {e}")

    def sendWaveform(self, plate, waveform):
        plate_index = np.where(QuadPlateEntries == plate)[0][0]
        sg_index, ch = self.SG_plateIndices[plate_index]
        sg = self.SG_connection[sg_index]

        if not sg["device"]:
            print(f"Signal generator for the plate {plate} has not been initiated.\nInitiating...")
            self.initSG(sg)
        
        if np.max(np.abs(waveform)) < WaveformMaximumThreshold:
            raise Exception("Received a virutally empty signal! Please check the waveform again.")
        
        try:
            chData = "\n".join(np.char.mod("%f", waveform)) # Convert to the string format which SG can read.
            wvName = datetime.now().strftime("%H%M%S") # Waveform name: current time.
            wvSlot = "50" if ch == 1 else "51" # Waveform slot: M50 for ch1 and M51 for ch2.

            sendCommand(sg["device"], f"C{ch}:BSWV WVTP, SINE")
            sendCommand(sg["device"], f"WVCSV M{wvSlot}, WAVENM, {wvName}, CSVLENG, 10000, CSVDATA,Amplitude\n{chData}", sleepTime=1)
            sendCommand(sg["device"], f"C{ch}:ARWV INDEX, {wvSlot}, NAME, {wvName}")
            sendCommand(sg["device"], f"C{ch}:BSWV FRQ, {self.arwvFreq}, OFST, 0")
            sendCommand(sg["device"], f"C{ch}:BTWV STATE, ON, GATE_NCYC, NCYC, TRSR, EXT")
            sendCommand(sg["device"], f"C{ch}:BTWV STATE, ON, GATE_NCYC, GATE, PLRT, POS")
        
        except Exception as e:
            print(f"Exception has occurred: {e}")

    def turnOutput(self, plate, target):
        plate_index = np.where(QuadPlateEntries == plate)[0][0]
        sg_index, ch = self.SG_plateIndices[plate_index]
        sg = self.SG_connection[sg_index]

        if not sg["device"]:
            print(f"Signal generator for the plate {plate} has not been initiated.\nInitiating...")
            self.initSG(sg)
        
        try:
            sendCommand(sg["device"], f"C{ch}:OUTP {target}")

        except Exception as e:
            print(f"Exception has occurred: {e}")
        

if __name__ == "__main__":
    server = SGBackendServer()



