import socket
import select

host = "0.0.0.0"
port = 5050

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

server_socket.bind((host, port))

server_socket.listen()

while True:
    # data = client_socket.recv(1024)
    # if not data:
    #     break
    # print(f"Received from {addr}: {data.decode()}")
    # client_socket.sendall(data)

    ready_to_read, ready_to_write, in_error = select.select([server_socket], [server_socket], [])
    
    client_socket, addr = server_socket.accept()
    print(f"Connected by {addr}")
    


    # try:
    #     ready_to_read, ready_to_write, in_error = select.select([client_socket,], [client_socket,], [], 5)
    # except select.error:
    #     client_socket.shutdown(2) # 0: done receiving, 1: done sending, 2: both
    #     client_socket.close()
    #     print("Connection error.")
    #     break
    
    if len(ready_to_read) > 0:
        while True:
            data = client_socket.recv(2048)
            print(f"Received from {addr}: {data.decode()}")
            if not data:
                break
            # if data == "quit":
            #     break
            if data.decode() == "quit":
                break
    
    client_socket.sendall(data)
    print(ready_to_write)
    if len(ready_to_write) > 0:
        client_socket.sendall("Some stuff.")
    
    break
    

client_socket.close()
server_socket.close()