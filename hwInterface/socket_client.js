const net = require('net');
const socket = net.connect({
    //host: '0.0.0.0',
    host: '143.248.185.74',
    port: 5050
});

socket.setEncoding('utf-8');

socket.on('connect', ()=>{
    console.log('Connected to the server.');

    setTimeout(()=>{
        socket.write('shutdownServer');
    }, 1000);
});

socket.on('data', data=>{
    console.log(data);
});

socket.on('close', ()=>{
    console.log('close');
});

socket.on('error', err=>{
    console.log(`On error: ${err.code}`);
});
