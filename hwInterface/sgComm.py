#! /usr/bin/python3

import pyvisa
from time import sleep

def main():
	rm = pyvisa.ResourceManager()
	resources = rm.list_resources()
	print(resources)

	sg = rm.open_resource(resources[0])

	def sendCmd(sg, cmd, read=True):
		sg.write(cmd)
		sleep(0.5)
		if read:
			outp = sg.read()
			print(outp)
			return outp

	sendCmd(sg, "*IDN?")

	sendCmd(sg, "C1:OUTP ON", read=False)
	sendCmd(sg, "C1:OUTP?")

	rm.close()

if __name__ == "__main__":
	main()
