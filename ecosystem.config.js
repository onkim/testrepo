module.exports = {
	apps: [
		{
			name: 'http', 							// pm2 name
			script: './httpServer/bin/www',			// Application script
			max_memory_restart: '200M',				// Restart if the memory allocation exceeds this
			watch: ['./httpServer/bin',				// Restart if any of these files has been modified
					'./httpServer/app.js',
					'./httpServer/routes',
					'./httpServer/utils',
					'./httpServer/views'
			],								
			watch_delay: 1000,						// Delay between restart
			env: {									// Environment variables
				NODE_ENV: 'development',
				role: 'main'
			}
		},
	]
};

